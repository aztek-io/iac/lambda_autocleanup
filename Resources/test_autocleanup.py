#!/usr/bin/env python

import unittest
import autocleanup
import datetime
from dateutil.tz import tzutc

class TestScript(unittest.TestCase):
    def test_custom_json_parser(self):
        self.assertEqual(
            autocleanup.custom_json_parser(
                datetime.datetime(2019, 8, 26, 14, 58, 22, tzinfo=tzutc())
            ),
            '2019-08-26T14:58:22+00:00'
        )

        with self.assertRaises(TypeError):
            autocleanup.custom_json_parser(10)

        with self.assertRaises(TypeError):
            autocleanup.custom_json_parser('asdf')


if __name__ == '__main__':
    unittest.main()
