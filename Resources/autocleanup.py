#!/usr/bin/env python
# Written by: Robert J.

import logging
import boto3
import os
import sys
import json
import re

from datetime import date, datetime

#######################################
### Logging Settings ##################
#######################################

logger = logging.getLogger()
logger.setLevel(logging.INFO)

#######################################
### Boto3 Configs #####################
#######################################

ec2         = boto3.client('ec2')
autoscaling = boto3.client('autoscaling')
sns         = boto3.client('sns')
iam         = boto3.client('iam')
sts         = boto3.client('sts')

#######################################
### Global Vars #######################
#######################################

TOPIC_ARN   = os.environ['TOPIC_ARN']

#######################################
### Main Function #####################
#######################################

def main(event, context):
    logger.info('Event: {}'.format(event))
    account             = get_aws_account()
    autoscaling_groups  = check_ec2_asg()
    ec2_ids             = check_ec2()

    send_message(account, autoscaling_groups, ec2_ids)


#######################################
### Generic Functions #################
#######################################

def get_aws_account(iam=iam, sts=sts):
    aws_account_number = sts.get_caller_identity().get('Account')

    try:
        aws_account = iam.list_account_aliases()["AccountAliases"][0]
    except IndexError as e:
        logger.info('No account alias seems to be set: {}'.format(e))
        aws_account = aws_account_number

    return aws_account


def type_error(error):
    raise TypeError(error)

def custom_json_parser(value):
    return value.isoformat() if isinstance(value, (datetime, date)) \
        else type_error('Type "{}" not serializable'.format(type(value)))


#######################################
### Program Specific Functions ########
#######################################

def check_ec2_asg():
    logger.info('Getting Autoscaling Group List.')

    resp = autoscaling.describe_tags()
    tags = list()

    while True:
        tags += resp['Tags']

        if 'NextToken' not in resp.keys():
            break

        resp = autoscaling.describe_tags(
            NextToken=resp['NextToken']
        )

    simplified = [
        {
            'id': tag['ResourceId'],
            'tags': {
                'key': tag['Key'],
                'value': tag['Value']
            }
        } for tag in tags
    ]

    logger.debug('Tags found: {}'.format(json.dumps(simplified, indent=4)))

    autoscaling_groups = [
        autoscaling_group['ResourceId'] for autoscaling_group in tags if autoscaling_group['Key'] == 'AutoCleanup' and autoscaling_group['Value'] == "true"
    ]

    logger.debug(
        'Auto Scaling Groups found: {autoscaling_groups}'.format(
            autoscaling_groups=json.dumps(
                autoscaling_groups,
                sort_keys=True,
                indent=4
            )
        )
    )

    if autoscaling_groups:
        logger.info('Scaling down Autoscaling Groups.')
        scale_down_autoscaling_groups(autoscaling_groups)

        return autoscaling_groups
    else:
        logger.debug('No Autoscaling Groups found.')
        return None


def check_ec2():
    filters = [
        {
            'Name': 'instance-state-name',
            'Values': [
                'running'
            ]
        },
        {
            'Name': 'tag:AutoCleanup',
            'Values': [
                "true"
            ]
        }
    ]

    logger.debug('Getting instance list with the following filter: {}'.format(json.dumps(filters, indent=4)))

    instances   = lookup_instance_data(filters)
    ids         = get_ids(instances)

    if ids:
        stop_instances(ids)

        logger.info('Instances stopped.')
        return ids
    else:
        logger.info('No instances found.')
        return None


def scale_down_autoscaling_groups(autoscaling_groups):
    for autoscaling_group in autoscaling_groups:
        logger.info('Setting {} to 0 desired instances.'.format(autoscaling_group))

        autoscaling.update_auto_scaling_group(
            AutoScalingGroupName=autoscaling_group,
            MinSize=0,
            MaxSize=0,
            DesiredCapacity=0
        )


def lookup_instance_data(filters):
    data        = ec2.describe_instances()

    # https://stackoverflow.com/a/11934483/6382401
    flat_data   = [
        instance for reservation in [
            r['Instances'] for r in data['Reservations']
        ] for instance in reservation
    ]

    logger.debug('Instances: {}'.format(json.dumps(flat_data, indent=4, default=custom_json_parser)))

    return flat_data


def get_ids(instances):
    ids = [
        i['InstanceId'] for i in instances if 'InstanceLifecycle' not in i.keys()
    ]

    logger.info('InstanceIds: {}'.format(ids))

    return ids


def stop_instances(ids):
    logger.info('Stopping instances.')

    resp = ec2.stop_instances(InstanceIds=ids)

    logger.info('Response: {}'.format(resp))


def send_message(account, autoscaling_groups, ec2_ids, topic_arn=TOPIC_ARN, sns=sns):
    if autoscaling_groups or ec2_ids:
        email_title = 'Lambda Infrastructure Control Enacted - ' + account

        email_body  = '''
        Auto Scaling Groups affected:

        {}

        EC2 Instances Affected:

        {}'''.format(json.dumps(autoscaling_groups, indent=4), ec2_ids)

        email_body = re.sub(r'\ {8}', '', email_body.strip('\n'))

        logger.info('Sending message to: {}'.format(topic_arn))
        logger.info('Message title: {}'.format(email_title))
        logger.info('Message body:\n{}'.format(email_body))

        resp = sns.publish(
            TopicArn=topic_arn,
            Message=email_body,
            Subject=email_title,
            MessageStructure='string'
        )

        logger.info('Response: {}'.format(resp))
    else:
        logger.info('No message necessary.')


#######################################
### Execution #########################
#######################################

def lambda_handler(event, context):
    main(event, context)


if __name__ == '__main__':
    stream = logging.StreamHandler(sys.stdout)

    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    stream.setFormatter(formatter)

    logger.addHandler(stream)

    event = {
        "something": "witty"
    }

    main(event, None)
