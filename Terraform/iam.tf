########################################
### IAM Policies #######################
########################################

resource "aws_iam_policy" "autocleanup" {
    name    = "autocleanup"
    path    = "/"
    policy  = <<-POLICY
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Effect": "Allow",
                "Action": [
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:PutLogEvents"
                ],
                "Resource": "arn:aws:logs:*:*:*"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "ec2:DescribeInstances",
                    "ec2:StopInstances",
                    "autoscaling:DescribeTags",
                    "iam:ListAccountAliases"
                ],
                "Resource": "*"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "autoscaling:UpdateAutoScalingGroup"
                ],
                "Resource": "arn:aws:autoscaling:*:*:autoScalingGroup:*:autoScalingGroupName/*"
            },
            {
                "Effect": "Allow",
                "Action": [
                    "sns:Publish"
                ],
                "Resource": "*"
            }
        ]
    }
    POLICY
}

########################################
### IAM Roles ##########################
########################################

resource "aws_iam_role" "autocleanup" {
    name = "autocleanup"

    assume_role_policy = <<-POLICY
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "sts:AssumeRole",
                "Principal": {
                    "Service": "lambda.amazonaws.com"
                },
                "Effect": "Allow",
                "Sid": ""
            }
        ]
    }
    POLICY
}

########################################
### IAM Policy Attachments #############
########################################

resource "aws_iam_role_policy_attachment" "autocleanup" {
    role        = aws_iam_role.autocleanup.name
    policy_arn  = aws_iam_policy.autocleanup.arn
}
