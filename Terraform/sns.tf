########################################
### SNS Topics #########################
########################################

resource "aws_sns_topic" "autocleanup" {
    name = join("-", [var.global["project"], var.environment, "autocleanup"])
}
