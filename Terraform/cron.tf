resource "aws_cloudwatch_event_rule" "cron_12AM_Everyday" {
    name                = join("-", [var.global["project"], var.environment, "12AM_Everyday"])
    description         = "Run at 12 AM CST Everyday"

    schedule_expression = "cron(00 5 * * ? *)"
}

resource "aws_lambda_permission" "cron" {
    action              = "lambda:InvokeFunction"
    function_name       = aws_lambda_function.autocleanup.arn
    principal           = "events.amazonaws.com"
    source_arn          = aws_cloudwatch_event_rule.cron_12AM_Everyday.arn
}

resource "aws_cloudwatch_event_target" "autocleanup" {
    rule                = aws_cloudwatch_event_rule.cron_12AM_Everyday.name
    target_id           = local.name
    arn                 = aws_lambda_function.autocleanup.arn
}
