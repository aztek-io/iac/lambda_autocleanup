########################################
### Lambda Configs: ####################
########################################

# lambda_shutdown_ec2
####################

data "archive_file" "zip_shutdown_ec2" {
    type            = "zip"

    source_file     = "${path.module}/../Resources/autocleanup.py"
    output_path     = "${path.module}/../Resources/autocleanup.zip"
}

resource "aws_lambda_function" "autocleanup" {
    depends_on      = [
        "data.archive_file.zip_shutdown_ec2"
    ]

    filename        = "${path.module}/../Resources/autocleanup.zip"
    function_name   = local.name
    role            = aws_iam_role.autocleanup.arn
    handler         = "autocleanup.lambda_handler"
    runtime         = "python3.7"
    memory_size     = 128
    timeout         = 15
    environment {
        variables   = {
            TOPIC_ARN = aws_sns_topic.autocleanup.arn
        }
    }
}
