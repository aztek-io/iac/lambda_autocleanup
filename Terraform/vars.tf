########################################
### Variables ##########################
########################################

variable "global" {
    type    = "map"
    default = {
        project     = "aztek"
        region      = "us-west-2"
    }
}

variable "environment" {}

variable "lambda" {
    type    = "map"
    default = {
        name    = "autocleanup"
    }
}

locals {
    name = join("-", [var.global["project"], var.environment, var.lambda["name"]])
}
########################################
### Data ###############################
########################################

data "aws_iam_account_alias" "current" {}
