terraform {
  backend "s3" {
    bucket  = "aztek.terraform.tfstate"
    key     = "autocleanup/terraform.tfstate"
    region  = "us-west-2"
    encrypt = "true"
  }
}
