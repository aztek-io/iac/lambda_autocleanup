# Documentation

This lambda function is essentially a Roomba with a knife.  Be sure you understand what this function is doing before you start using it.

## How is this used?

Configure a tag called "AutoCleanup" on your EC2 instances and Auto Scaling Groups, with a value of "true" or "false".

Resources with a value of "true" will be cleaned up.

## What resources are affected?

Right now:
  * ec2: instances are stopped
  * ec2 autoscaling groups: desired, and max counts are set to 0

In the future (next time I am creating temporary resources for contracting):
  * ELBs
  * NAT Gateways
  * Other overpriced infrastructure offered by amazon

